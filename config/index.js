import * as urls from "./urls";
import * as statuses from "./statuses";

export const LANGUAGES = ["en", "kg", "cn"];

export { urls, statuses };
