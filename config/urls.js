export const ARTICLES_ENDPOINT = `/api/articles/`;
export const ARTICLES_AUTHORS_ENDPOINT = `/api/article-authors/`;
export const CATEGORIES_ENDPOINT = `/api/categories/`;
export const CONTACTS_ENDPOINT = `/api/contacts/`;
export const PAGES_ENDPOINT = `/api/pages/`;
export const PRODUCTS_ENDPOINT = `/api/products/`;
export const REVIEWS_ENDPOINT = `/api/reviews/`;
export const SLIDES_ENDPOINT = `/api/slides/`;
//
export const ME_ENDPOINT = `/api/auth/me/`;
export const LOGIN_ENDPOINT = `/api/auth/login/`;
//
export const SETTINGS_ENDPOINT = `/api/settings/properties`;
// forms
export const CART_ORDER_ENDPOINT = `/api/notifications/cart`;
export const ONECLICK_ORDER_ENDPOINT = `/api/notifications/oneclick`;
export const CALLME_ORDER_ENDPOINT = `/api/notifications/callme`;
