const path = require("path");
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
    .BundleAnalyzerPlugin;

const isProduction = process.env.NODE_ENV === "production";
const withAnalyze = process.env.ANALYZE || false;

module.exports = {
    mode: isProduction ? "production" : "development",
    entry: "./index.js",
    target: "node",
    output: {
        path: path.resolve(__dirname, "dist"),
        library: "kyrgyzhoney-mobx",
        libraryTarget: "umd"
    },
    module: {
        rules: [
            {
                test: /.jsx?$/,
                loader: "babel-loader",
                exclude: /node_modules/
            }
        ]
    },
    resolve: {
        symlinks: true
    },
    plugins: withAnalyze ? [new BundleAnalyzerPlugin()] : []
};
