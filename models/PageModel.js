import { types } from "mobx-state-tree";
import { createModel, translateField } from "redline-mobx";
import { urls, LANGUAGES } from "../config";
import UploadsItem from "./UploadsItem";
import MetaModel from "./MetaModel";

const PageModel = createModel({
    type: types.model({
        ...translateField(types.string, "title", LANGUAGES),
        slug: types.string,
        ...translateField(types.string, "alt_title", LANGUAGES),
        is_about_company: types.boolean,
        header_background: types.maybeNull(UploadsItem),
        ...translateField(types.string, "body", LANGUAGES)
    }),
    endpoint: urls.PAGES_ENDPOINT,
    name: "PageModel",
    mixin_types: [MetaModel]
});

export default PageModel;
