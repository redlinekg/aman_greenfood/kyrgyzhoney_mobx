import { types } from "mobx-state-tree";
import { createModel, translateField } from "redline-mobx";
import { urls, LANGUAGES } from "../config";

const CategoryModel = createModel({
    type: types.model({
        ...translateField(types.string, "title", LANGUAGES),
        slug: types.string,
        ...translateField(types.string, "alt_title", LANGUAGES),
        ...translateField(types.string, "description", LANGUAGES),
        products: types.late(() => {
            const ProductModel = require("./ProductModel");
            return types.optional(types.array(ProductModel.default), []);
        })
    }),
    endpoint: urls.CATEGORIES_ENDPOINT,
    name: "CategoryModel"
});

export default CategoryModel;
