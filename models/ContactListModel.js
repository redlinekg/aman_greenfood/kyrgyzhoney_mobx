import { types } from "mobx-state-tree";
import { createModel, translateField } from "redline-mobx";
import { urls, LANGUAGES } from "../config";

const ContactListModel = createModel({
    type: types.model({
        type: types.string,
        ...translateField(types.string, "text", LANGUAGES)
    }),
    name: "ContactListModel"
});

export default ContactListModel;
