import { types } from "mobx-state-tree";
import { createModel, translateField } from "redline-mobx";
import { LANGUAGES } from "../config";

const ProductPropertiesModel = createModel({
    type: types.model({
        ...translateField(types.string, "type", LANGUAGES),
        ...translateField(types.string, "value", LANGUAGES)
    }),
    name: "ProductPropertiesModel"
});

export default ProductPropertiesModel;
