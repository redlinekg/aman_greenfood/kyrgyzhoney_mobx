import { types, getSnapshot } from "mobx-state-tree";
import { createModel, translateField } from "redline-mobx";
import { urls, LANGUAGES } from "../config";
import CategoryModel from "./CategoryModel";
import ProductPropertiesModel from "./ProductPropertiesModel";
import UploadsItem from "./UploadsItem";
import MetaModel from "./MetaModel";
import { getStore } from "../index";

const ProductModel = createModel({
    type: types.model({
        ...translateField(types.string, "title", LANGUAGES),
        slug: types.string,
        category: types.maybeNull(CategoryModel),
        image: types.maybeNull(UploadsItem),
        ...translateField(types.string, "description", LANGUAGES),
        ...translateField(types.string, "volume", LANGUAGES),
        pin: types.boolean,
        header_background: types.maybeNull(UploadsItem),
        ...translateField(types.maybeNull(types.string), "body", LANGUAGES),
        properties: types.array(ProductPropertiesModel),
        video: types.maybeNull(UploadsItem),
        images: types.array(UploadsItem)
    }),
    endpoint: urls.PRODUCTS_ENDPOINT,
    name: "ProductModel",
    mixin_types: [MetaModel]
}).actions(self => ({
    cartIncrease() {
        const cart_store = getStore("cart_store");
        cart_store.increaseLine(getSnapshot(self));
    },
    cartDecrease() {
        const cart_store = getStore("cart_store");
        cart_store.decreaseLine(getSnapshot(self));
    },
    setCartCount(count) {
        const cart_store = getStore("cart_store");
        cart_store.setCountForProduct(getSnapshot(self), count);
    },
    cartRemove() {
        const cart_store = getStore("cart_store");
        cart_store.removeLine(getSnapshot(self));
    }
}));

export default ProductModel;
