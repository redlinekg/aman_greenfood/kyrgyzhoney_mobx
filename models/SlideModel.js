import { types } from "mobx-state-tree";
import { createModel, translateField } from "redline-mobx";
import { urls, LANGUAGES } from "../config";
import UploadsItem from "./UploadsItem";

const SlideModel = createModel({
    type: types.model({
        ...translateField(types.string, "title", LANGUAGES),
        ...translateField(types.string, "title2", LANGUAGES),
        ...translateField(types.string, "sub_title", LANGUAGES),
        link: types.maybeNull(types.string),
        ...translateField(
            types.maybeNull(types.string),
            "button_text",
            LANGUAGES
        ),
        ...translateField(types.string, "description", LANGUAGES),
        image: UploadsItem,
        background_image: types.maybeNull(UploadsItem),
        video: types.maybeNull(UploadsItem),
        background_color: types.maybeNull(types.string)
    }),
    endpoint: urls.SLIDES_ENDPOINT,
    name: "SlideModel"
});

export default SlideModel;
