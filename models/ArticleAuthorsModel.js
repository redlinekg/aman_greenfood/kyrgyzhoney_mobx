import { types } from "mobx-state-tree";
import { createModel, translateField } from "redline-mobx";
import { urls, LANGUAGES } from "../config";
import UploadsItem from "./UploadsItem";

const ArticleAuthorsModel = createModel({
    type: types.model({
        ...translateField(types.string, "title", LANGUAGES),
        avatar: types.maybeNull(UploadsItem)
    }),
    endpoint: urls.ARTICLES_AUTHORS_ENDPOINT,
    name: "ArticleAuthorsModel"
});

export default ArticleAuthorsModel;
