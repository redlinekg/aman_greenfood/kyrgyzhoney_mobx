import { types } from "mobx-state-tree";
import { translateField } from "redline-mobx";
import UploadsItem from "./UploadsItem";
import { LANGUAGES } from "../config";

const MetaModel = types.model("MetaModel", {
    ...translateField(types.maybeNull(types.string), "meta_title", LANGUAGES),
    ...translateField(
        types.maybeNull(types.string),
        "meta_description",
        LANGUAGES
    ),
    meta_image: types.maybeNull(UploadsItem)
});
export default MetaModel;
