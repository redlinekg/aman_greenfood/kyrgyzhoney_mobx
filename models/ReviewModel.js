import { types } from "mobx-state-tree";
import { createModel, translateField } from "redline-mobx";
import { urls, LANGUAGES } from "../config";
import UploadsItem from "./UploadsItem";

const ReviewModel = createModel({
    type: types.model({
        avatar: types.maybeNull(UploadsItem),
        ...translateField(types.string, "name", LANGUAGES),
        ...translateField(types.string, "country", LANGUAGES),
        social_type: types.maybeNull(types.string),
        social_link: types.maybeNull(types.string),
        ...translateField(types.string, "title", LANGUAGES),
        ...translateField(types.string, "body", LANGUAGES)
    }),
    endpoint: urls.REVIEWS_ENDPOINT,
    name: "ReviewModel"
});

export default ReviewModel;
