import { types } from "mobx-state-tree";
import { createModel, translateField } from "redline-mobx";
import { urls, LANGUAGES } from "../config";
import ArticleAuthorsModel from "./ArticleAuthorsModel";
import UploadsItem from "./UploadsItem";
import MetaModel from "./MetaModel";

const ArticleSiblink = types.maybeNull(types.model("ArticleSiblink", {
    slug: types.string,
    title: types.string,
}));

const ArticleModel = createModel({
    type: types.model({
        ...translateField(types.string, "title", LANGUAGES),
        slug: types.string,
        author: ArticleAuthorsModel,
        ...translateField(types.string, "description", LANGUAGES),
        ...translateField(types.string, "body", LANGUAGES),
        pin: types.boolean,
        image: UploadsItem,
        header_background: types.maybeNull(UploadsItem),
        prev_entity: ArticleSiblink,
        next_entity: ArticleSiblink,
    }),
    endpoint: urls.ARTICLES_ENDPOINT,
    name: "ArticleModel",
    mixin_types: [MetaModel],
});

export default ArticleModel;
