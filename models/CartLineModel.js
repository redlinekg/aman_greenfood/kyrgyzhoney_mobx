import { types } from "mobx-state-tree";
import ProductModel from "./ProductModel";

const CartLineModel = types.model("CartLineModel", {
    product: ProductModel,
    quantity: types.number
});

export default CartLineModel;
