import { types } from "mobx-state-tree";
import { createModel, translateField } from "redline-mobx";
import { urls, LANGUAGES } from "../config";
import ContactListModel from "./ContactListModel";

const ContactModel = createModel({
    type: types.model({
        ...translateField(types.string, "title", LANGUAGES),
        global: types.boolean,
        items: types.array(ContactListModel)
    }),
    endpoint: urls.CONTACTS_ENDPOINT,
    name: "ContactModel"
});

export default ContactModel;
