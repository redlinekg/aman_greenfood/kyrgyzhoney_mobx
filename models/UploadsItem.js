import { types } from "mobx-state-tree";
import { createModel } from "redline-mobx";

const UploadsItem = createModel({
    type: types.model({
        id: types.maybeNull(types.number),
        created_at: types.maybeNull(types.string),
        updated_at: types.maybeNull(types.string),
        isImage: types.optional(types.boolean, false),
        path: types.string,
        absolute_path: types.string,
        url_path: types.string
    }),
    name: "UploadsItem"
});

export default UploadsItem;
