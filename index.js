import {
    ArticlesStore,
    ArticleStore,
    ArticleAuthorsStore,
    ArticleAuthorStore,
    CategoriesStore,
    CategoryStore,
    ContactsStore,
    ContactStore,
    PagesStore,
    PageStore,
    ProductsStore,
    ProductStore,
    ReviewsStore,
    ReviewStore,
    SlidesStore,
    SlideStore,
    //
    AuthStore,
    SettingsStore,
    //
    CallmeFormStore,
    CartFormStore,
    //
    CartStore
} from "./stores";
export * from "./stores";
export * from "./models";
import { RedlineStores } from "redline-mobx";

let last_stores = null;

export const createStores = ({
    snapshots = {},
    cookie = null,
    logger,
    axios: original_axios,
    baseURL
} = {}) => {
    if (!baseURL) {
        logger.error("you must to provide baseURL to 'createStores'");
    }
    const axios = original_axios.create({
        headers: cookie
            ? {
                  Cookie: cookie
              }
            : {},
        withCredentials: true,
        baseURL: baseURL
    });
    axios.interceptors.request.use(request => {
        logger.debug(`Axios request: ${request.url}`);
        return request;
    });

    const stores = new RedlineStores(logger, axios, snapshots);
    stores.add({
        name: "article_store",
        type: ArticleStore
    });
    stores.add({
        name: "articles_store",
        type: ArticlesStore
    });
    stores.add({
        name: "article_authors_store",
        type: ArticleAuthorsStore
    });
    stores.add({
        name: "article_author_store",
        type: ArticleAuthorStore
    });
    stores.add({
        name: "categories_store",
        type: CategoriesStore
    });
    stores.add({
        name: "category_store",
        type: CategoryStore
    });
    stores.add({
        name: "contacts_store",
        type: ContactsStore
    });
    stores.add({
        name: "contact_store",
        type: ContactStore
    });
    stores.add({
        name: "pages_store",
        type: PagesStore
    });
    stores.add({
        name: "page_store",
        type: PageStore
    });
    stores.add({
        name: "products_store",
        type: ProductsStore
    });
    stores.add({
        name: "product_store",
        type: ProductStore
    });
    stores.add({
        name: "reviews_store",
        type: ReviewsStore
    });
    stores.add({
        name: "review_store",
        type: ReviewStore
    });
    stores.add({
        name: "slides_store",
        type: SlidesStore
    });
    stores.add({
        name: "slide_store",
        type: SlideStore
    });
    //
    stores.add({
        name: "auth_store",
        type: AuthStore,
        snapshot: { user: null }
    });
    stores.add({
        name: "settings_store",
        type: SettingsStore,
        snapshot: {}
    });
    stores.add({
        name: "cart_store",
        type: CartStore,
        snapshot: {}
    });
    //
    stores.add({
        name: "callme_form_store",
        type: CallmeFormStore
    });
    stores.add({
        name: "cart_form_store",
        type: CartFormStore
    });
    last_stores = stores;
    return last_stores;
};

export function getStore(name) {
    if (last_stores) {
        const store = last_stores.getStore(name);
        return store;
    }
}
