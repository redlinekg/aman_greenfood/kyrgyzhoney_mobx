import { createDetailStore } from "redline-mobx";
import { urls } from "../config";
import { ProductModel } from "../models";

const ProductStore = createDetailStore({
    type: ProductModel,
    name: "ProductStore",
    endpoint: urls.PRODUCTS_ENDPOINT
});

export default ProductStore;
