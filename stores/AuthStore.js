import { types, getEnv } from "mobx-state-tree";
import { urls } from "../config";
import Cookies from "browser-cookies";

const UserModel = types.model("UserModel", {
    id: types.maybeNull(types.number),
    email: types.maybeNull(types.string),
    password: types.maybeNull(types.string),
});

const AuthStore = types
    .model("AuthStore", {
        user: types.maybeNull(UserModel),
    })
    .actions((self) => ({
        async login({ email, password }) {
            const response = await getEnv(self).axios.post(
                urls.LOGIN_ENDPOINT,
                {
                    email,
                    password,
                }
            );
            Cookies.set("token", response.data);
            return response.data;
        },
        logout() {
            Cookies.erase("token");
            self.user = null;
        },
        setUserData(data) {
            self.user = UserModel.create(data);
        },
        async getMe() {
            const { logger, axios } = getEnv(self);
            logger.debug("Start `getMe`");
            try {
                const response = await axios.get(urls.ME_ENDPOINT);
                if (response.data) {
                    self.setUserData(response.data);
                }
            } catch (error) {
                logger.error(error.message);
                logger.debug("Fail try to `getMe`");
            }
        },
    }))
    .views((self) => ({
        isAuthenticated() {
            return !!self.user;
        },
    }));

export default AuthStore;
