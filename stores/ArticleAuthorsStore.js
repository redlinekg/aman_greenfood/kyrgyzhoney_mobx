import { createListStore } from "redline-mobx";
import { urls } from "../config";
import { ArticleAuthorsModel } from "../models";

const ArticleAuthorsStore = createListStore({
    type: ArticleAuthorsModel,
    name: "ArticleAuthorsStore",
    endpoint: urls.ARTICLES_AUTHORS_ENDPOINT
});

export default ArticleAuthorsStore;
