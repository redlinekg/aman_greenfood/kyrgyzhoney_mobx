import { createListStore } from "redline-mobx";
import { urls } from "../config";
import { ProductModel } from "../models";

const ProductsStore = createListStore({
    type: ProductModel,
    name: "ProductsStore",
    endpoint: urls.PRODUCTS_ENDPOINT
});

export default ProductsStore;
