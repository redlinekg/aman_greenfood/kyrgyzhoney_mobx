import { createListStore } from "redline-mobx";
import { urls } from "../config";
import { PageModel } from "../models";

const PagesStore = createListStore({
    type: PageModel,
    endpoint: urls.PAGES_ENDPOINT,
    name: "PagesStore"
});

export default PagesStore;
