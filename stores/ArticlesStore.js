import { createListStore } from "redline-mobx";
import { urls } from "../config";
import { ArticleModel } from "../models";

const ArticlesStore = createListStore({
    type: ArticleModel,
    name: "ArticlesStore",
    endpoint: urls.ARTICLES_ENDPOINT
});

export default ArticlesStore;
