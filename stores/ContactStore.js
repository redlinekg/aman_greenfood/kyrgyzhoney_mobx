import { createDetailStore } from "redline-mobx";
import { urls } from "../config";
import { ContactModel } from "../models";

const ContactStore = createDetailStore({
    type: ContactModel,
    name: "ContactStore",
    endpoint: urls.CONTACTS_ENDPOINT
});

export default ContactStore;
