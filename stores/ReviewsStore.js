import { createListStore } from "redline-mobx";
import { urls } from "../config";
import { ReviewModel } from "../models";

const ReviewsStore = createListStore({
    type: ReviewModel,
    endpoint: urls.REVIEWS_ENDPOINT,
    name: "ReviewsStore"
});

export default ReviewsStore;
