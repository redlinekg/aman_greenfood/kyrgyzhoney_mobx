import { createFormStore } from "redline-mobx";
import { urls } from "../config";

const CartFormStore = createFormStore({
    name: "CartFormStore",
    endpoint: urls.CART_ORDER_ENDPOINT
});

export default CartFormStore;
