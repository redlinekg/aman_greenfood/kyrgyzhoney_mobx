import { createListStore } from "redline-mobx";
import { urls } from "../config";
import { ContactModel } from "../models";

const ContactsStore = createListStore({
    type: ContactModel,
    name: "ContactsStore",
    endpoint: urls.CONTACTS_ENDPOINT
});

export default ContactsStore;
