import { createFormStore } from "redline-mobx";
import { urls } from "../config";

const CallmeFormStore = createFormStore({
    name: "CallmeFormStore",
    endpoint: urls.CALLME_ORDER_ENDPOINT
});

export default CallmeFormStore;
