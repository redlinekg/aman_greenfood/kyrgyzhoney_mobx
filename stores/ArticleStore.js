import { createDetailStore } from "redline-mobx";
import { urls } from "../config";
import { ArticleModel } from "../models";

const ArticleStore = createDetailStore({
    type: ArticleModel,
    name: "ArticleStore",
    endpoint: urls.ARTICLES_ENDPOINT
});

export default ArticleStore;
