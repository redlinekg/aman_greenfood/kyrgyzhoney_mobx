import { types, getEnv } from "mobx-state-tree";
import { urls } from "../config";
import { BaseStore } from "redline-mobx";

const SettingsStore = types
    .compose(
        "SettingsStore",
        BaseStore,
        types.model({
            //Social
            YOUTUBE: types.optional(types.string, ""),
            TWITTER: types.optional(types.string, ""),
            INSTAGRAM: types.optional(types.string, ""),
            FACEBOOK: types.optional(types.string, ""),
            TELEGRAM: types.optional(types.string, ""),
            WHATSAPP: types.optional(types.string, ""),
            // Meta Index
            INDEX_META_TITLE_RU: types.optional(types.string, ""),
            INDEX_META_TITLE_EN: types.optional(types.string, ""),
            INDEX_META_TITLE_KG: types.optional(types.string, ""),
            INDEX_META_DESC_RU: types.optional(types.string, ""),
            INDEX_META_DESC_EN: types.optional(types.string, ""),
            INDEX_META_DESC_kG: types.optional(types.string, ""),
            // Meta Blog
            BLOG_META_TITLE_RU: types.optional(types.string, ""),
            BLOG_META_TITLE_EN: types.optional(types.string, ""),
            BLOG_META_TITLE_KG: types.optional(types.string, ""),
            BLOG_META_DESC_RU: types.optional(types.string, ""),
            BLOG_META_DESC_EN: types.optional(types.string, ""),
            BLOG_META_DESC_kG: types.optional(types.string, ""),
            // Other
            SERVICE_LINK: types.optional(types.string, ""),
            REGISTER_LINK: types.optional(types.string, ""),
            // Contacts
            EMAIL: types.optional(types.string, ""),
            PHONE_ONE: types.optional(types.string, ""),
            PHONE_TWO: types.optional(types.string, ""),
            PHONE_THREE: types.optional(types.string, ""),
            // Meta Contacts
            CONTACTS_META_TITLE_RU: types.optional(types.string, ""),
            CONTACTS_META_TITLE_EN: types.optional(types.string, ""),
            CONTACTS_META_TITLE_KG: types.optional(types.string, ""),
            CONTACTS_META_DESC_RU: types.optional(types.string, ""),
            CONTACTS_META_DESC_EN: types.optional(types.string, ""),
            CONTACTS_META_DESC_KG: types.optional(types.string, "")
        })
    )
    .actions(self => ({
        setData(data) {
            getEnv(self).logger.debug("[SettingsStore] setData;");
            Object.assign(
                self,
                data.reduce((acc, property) => {
                    acc[property.name] = property.value;
                    return acc;
                }, {})
            );
        },
        async getSettings() {
            self.setLoading();
            try {
                const result = await getEnv(self).axios.get(
                    urls.SETTINGS_ENDPOINT
                );
                if (result) {
                    self.setData(result.data);
                }
                self.setReady();
            } catch (error) {
                getEnv(self).logger.error(error.message);
                self.setError(error);
            }
        }
    }));

export default SettingsStore;
