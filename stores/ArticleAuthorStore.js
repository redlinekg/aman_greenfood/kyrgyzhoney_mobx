import { createDetailStore } from "redline-mobx";
import { urls } from "../config";
import { ArticleAuthorsModel } from "../models";

const ArticleAuthorStore = createDetailStore({
    type: ArticleAuthorsModel,
    name: "ArticleAuthorStore",
    endpoint: urls.ARTICLES_AUTHORS_ENDPOINT
});

export default ArticleAuthorStore;
