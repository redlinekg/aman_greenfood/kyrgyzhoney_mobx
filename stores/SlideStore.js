import { createDetailStore } from "redline-mobx";
import { urls } from "../config";
import { SlideModel } from "../models";

const SlideStore = createDetailStore({
    type: SlideModel,
    endpoint: urls.SLIDES_ENDPOINT,
    name: "SlideStore"
});

export default SlideStore;
