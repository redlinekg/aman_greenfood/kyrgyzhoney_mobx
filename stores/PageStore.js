import { createDetailStore } from "redline-mobx";
import { urls } from "../config";
import { PageModel } from "../models";

const PageStore = createDetailStore({
    type: PageModel,
    endpoint: urls.PAGES_ENDPOINT,
    name: "PageStore"
});

export default PageStore;
