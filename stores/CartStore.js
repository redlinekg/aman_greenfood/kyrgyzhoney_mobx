import { types, applySnapshot } from "mobx-state-tree";
import { CartLineModel } from "../models";

const isBrowser = () => typeof window !== "undefined";
let isFirst = true;

const CartStore = types
    .model("CartStore", {
        lines: types.optional(types.array(CartLineModel), []),
    })
    .views((self) => ({
        get count() {
            return self.lines.reduce((acc, cv) => acc + cv.quantity, 0);
        },
        getById(product_id) {
            return self.lines.find((line) => line.product.id === product_id);
        },
    }))
    .actions((self) => ({
        clean() {
            self.lines.clear();
        },
        removeLine(product_snapshot) {
            self.lines = self.lines.filter(
                (line) => line.product.id !== product_snapshot.id
            );
        },
        increaseLine(product_snapshot) {
            const already_added = self.getById(product_snapshot.id);
            if (already_added) {
                const next_quantity = already_added.quantity + 1;
                if (next_quantity > 0) {
                    already_added.quantity = next_quantity;
                }
            } else {
                self.lines.push(
                    CartLineModel.create({
                        product: product_snapshot,
                        quantity: 1,
                    })
                );
            }
        },
        decreaseLine(product_snapshot) {
            const already_added = self.getById(product_snapshot.id);
            if (already_added) {
                const next_quantity = already_added.quantity - 1;
                already_added.quantity = next_quantity;
            } else {
                self.lines.push(
                    CartLineModel.create({
                        product: product_snapshot,
                        quantity: 1,
                    })
                );
            }
        },
        setCountForProduct(product_snapshot, count) {
            const already_added = self.getById(product_snapshot.id);
            if (already_added) {
                already_added.quantity = count;
            } else {
                self.lines.push(
                    CartLineModel.create({
                        product: product_snapshot,
                        quantity: count,
                    })
                );
            }
        },
    }))
    .preProcessSnapshot((snapshot) => {
        if (isBrowser() && isFirst) {
            const cart_value = localStorage.getItem("cart");
            if (cart_value) {
                return JSON.parse(cart_value);
            }
        }
        isFirst = false;
        return snapshot;
    })
    .postProcessSnapshot((snapshot) => {
        if (isBrowser()) {
            localStorage.setItem("cart", JSON.stringify(snapshot));
        }
    });

export default CartStore;
