import { createDetailStore } from "redline-mobx";
import { urls } from "../config";
import { ReviewModel } from "../models";

const ReviewStore = createDetailStore({
    type: ReviewModel,
    endpoint: urls.REVIEWS_ENDPOINT,
    name: "ReviewStore"
});

export default ReviewStore;
