import { createDetailStore } from "redline-mobx";
import { urls } from "../config";
import { CategoryModel } from "../models";

const CategoryStore = createDetailStore({
    type: CategoryModel,
    name: "CategoryStore",
    endpoint: urls.CATEGORIES_ENDPOINT
});

export default CategoryStore;
