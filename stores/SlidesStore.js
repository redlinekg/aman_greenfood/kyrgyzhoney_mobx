import { createListStore } from "redline-mobx";
import { urls } from "../config";
import { SlideModel } from "../models";

const SlidesStore = createListStore({
    type: SlideModel,
    endpoint: urls.SLIDES_ENDPOINT,
    name: "SlidesStore"
});

export default SlidesStore;
