import { createListStore } from "redline-mobx";
import { urls } from "../config";
import { CategoryModel } from "../models";

const CategoriesStore = createListStore({
    type: CategoryModel,
    name: "CategoriesStore",
    endpoint: urls.CATEGORIES_ENDPOINT
});

export default CategoriesStore;
